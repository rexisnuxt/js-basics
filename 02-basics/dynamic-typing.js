let name = "Rex"; // String literal or a String
let age = 30; // Number literal
let isApproved = true; // Boolean literal
let fisrtName = undefined;
let selectedColor = null;

// Nulls are used to explicitly clear the value or a variable

console.log(typeof name); // can check the type of variable

name = 20;

console.log(typeof name);

console.log(typeof age);
age = 3.14;
console.log(typeof age);

console.log(typeof fisrtName);
console.log(typeof selectedColor);
