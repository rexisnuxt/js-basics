const person = {
  name: "Rex",
  age: 20,
};

// Objects contains key and value or key-value pairs
// Keys are the name of the properties
// Values are the values associated with the keys

// Working with objects
// Dot notation
person.name = "Vinxent";

// Bracket Notation
person["age"] = 21;

console.log(person);
