# 02 Basics

This section contains the very basic operations and functions that are used in the
JavaScript programming language such as variables, operators, decision making, and more.

## Variables

- Variables are used to store data temporarily.
- Storing the variable somewhere and giving that location a name
- The concept is similar to boxes with labels.

### Rules in naming variables

1. They cannot be a reserved keyword
2. They should be meaningful
3. They cannot start with a numbr
4. They cannot contain space or hyphen (-)
5. They are cased sensitive

## Constants

- Constants are immutable variables
- They values cannot be change on runtime
- You cannot reassign a constant
- If you don't need to reassign `constant` should be your default choice;
- Otherwise, use `let`

## Primitive Types

- Also called **value types**

| types       | examples                                                     |
| ----------- | ------------------------------------------------------------ |
| `String`    | `"Hello, World"`, `"JavaScript"`                             |
| `Number`    | `1, 2, 314`                                                  |
| `Boolean`   | `true` or `false`                                            |
| `undefined` | The default value of variables that are not initialized      |
| `null`      | Absence value. Used to explicitly clear value of a variable. |

## Dynamic Typing

- JavaScript is considered to be a dynamically typed language.
- `statically typed` means that the type of a variable cannot be change on runtime
- `dynamically typed` means that the type of a variable can be change on runtime
- We can use the `typeof` function to check the type of a variable.

## Reference Types

| types       | examples                                                 |
| ----------- | -------------------------------------------------------- |
| `Objects`   | Has properties. A way to group related objects together. |
| `Arrays`    | List of objects or items                                 |
| `Functions` | A group of statements that perform a particular action.  |

### Objects

- Contains `keys` and `values`
- `keys` are the properties
- `values` are the actual values associated with the property of key
- `{}` is the object literal syntax. It is used to create objects

#### Working with objects

1. Using the dot notation `.`
   - Should be the default choice when working with objects
   - Much consise and is easier to read
2. Using the bracket `[]`
   - Should only be used when we don't know the property that we are dealing with
   - Useful when selecting properties on runtime

### Arrays

- A collection of objects or items
- `[]` or array literal is used to initialized an empty array.
- Each item in the array has index, the first element starts at zero `0`
- Dynamic typing also applies to arrays.
- An array can contain multiple types of data

### Functions

- Group of statements that perform an action
- `function` keyword is used to declare a function
- `nameOfFunction` can be any but must follow naming rules same as variables.
- `parameterList` are special variables only accessible to the function. A function can have zero or more parameters.
- `{}` any code that is inside the curly braces is part of the body of the function

```js
// General structure of a function
function nameOfFunction(parameterList) {
  // Function body
}
```

#### Types of functions

1. `void` functions are functions that do not return anything and only perform actions
2. `value type` functions are functions that return a value.

#### Things to remember about functions

1. Parameters are optional
2. They do not need to be terminated with a semicolon `;`
