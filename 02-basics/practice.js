const person = {
  firstName: "",
  lastName: "",
  middleName: "",
  age: 0,
  contactNumber: "",

  getFullName: function () {
    return this.firstName + " " + this.middleName + " " + this.lastName;
  },
};

let rex = Object.create(person);
rex.firstName = "Rex Vinxent";
rex.lastName = "Culanag";

console.log(rex.getFullName());
