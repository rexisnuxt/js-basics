let x = 10;
let y = 3;

console.log(x + y); // Addition
// Expression is something that produces a value
console.log(x - y); // Subtraction
console.log(x * y); // Mutliplication
console.log(x / y); // Division
console.log(x % y); // Modulo or Remainder
console.log(x ** y); // Exponential

// Increment(++)
console.log(++x);
console.log(x++);

// Decrement(--)
console.log(--x);
console.log(x--);
