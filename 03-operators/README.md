# 03-operators

This sections contains lessons for the different operators that JavaScript has to offer such as logical, arithmetic, and bitwise operators.

## Operators

- Used with variables to create expressions
- This expressions are used to implement logic and algorithms

### Different types of operators

1. Arithmetic
2. Assignment
3. Comparison
4. Logical
5. Bitwise

## Arithmetic Operators

- Involves calculations
- Two operands combined with an operator produces an expression
- An **expression** is a statement that produces a value

| Operators | Examples                       | Definition                     |
| --------- | ------------------------------ | ------------------------------ |
| `+`       | `console.log(10 + 2); // 12`   | Addition                       |
| `-`       | `console.log(10 - 2); // 8`    | Subtraction                    |
| `*`       | `console.log(10 * 2); // 20`   | Multiplication                 |
| `/`       | `console.log(10 / 2); // 5`    | Division                       |
| `%`       | `console.log(11 % 2); // 1`    | Modulo (Returns the remainder) |
| `**`      | `console.log(10 ** 2); // 100` | Exponential                    |
| `++`      | `let x = 10;`<br>`x++;`        | Increment                      |
| `--`      | `let x = 10;`<br>`x--;`        | Decrement                      |

### Things to remember about `Increment` and `Decrement` operators

1. When used before an operand or a variable, the adds one (1) to the value of the variable first and then uses the value afterwards.

```js
let x = 10;
let z = 8;

// Post-increment
// Output to console => 10
// Actual value of the variable => 11
console.log(x++);

// Output to console => 11
console.log(x);

// Pre-increment
// Output to console => 9
// Actual value of the variable => 9
console.log(++z);

// Output to console => 9
console.log(z);
```

2. When used after an operand of a variable, the operator uses the current value of the value and then adds one (1) to it afterwards.

```js
let x = 10;
let z = 8;

// Post-decrement
// Output to console => 10
// Actual value of the variable => 9
console.log(x--);

// Output to console => 9
console.log(x);

// Pre-increment
// Output to console => 7
// Actual value of the variable => 7
console.log(--z);

// Output to console => 7
console.log(z);
```
