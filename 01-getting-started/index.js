// Statements are lines of code that do something
// This my first JavaScript code
console.log("Hello World");

// Separation of concerns
// Separating concepts and functionalities into their
// own modules of folders for better organization and
// maintainability in the future

// To run javascript files in node
// node <javascript-file.js>
